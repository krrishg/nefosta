from django.db import models

class User(models.Model):
    username = models.CharField(max_length=20, unique=True)
    password = models.CharField(max_length=100)
    email = models.CharField(max_length=100, unique=True)
    introduction = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=15, unique=True)
    profile_picture = models.CharField(max_length=30)
