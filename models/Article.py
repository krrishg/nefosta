from django.db import models

class Article(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    posted_on = models.DateField(default=datetime.now, blank=True)
    photo = models.CharField(max_length=30, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
