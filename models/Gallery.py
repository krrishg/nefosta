from django.db import models

class Gallery(models.Model):
    photo = models.CharField(max_length=30)
    album_name = models.CharField(max_length=30)
